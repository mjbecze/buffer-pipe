const tape = require('tape')
const Pipe = require('../')

tape('tests', t => {
  let p = new Pipe(Buffer.from([1, 2, 3, 4]))
  p.write(Buffer.from([99]))

  t.deepEquals(p.buffer, Buffer.from([1, 2, 3, 4, 99]))

  const buf = p.read(3)

  t.equals(p.bytesRead, 3, 'should record the number of bytes read from the pipe')
  t.deepEquals(buf, Buffer.from([1, 2, 3]))
  t.equal(p.end, false)

  p = new Pipe()
  p.write(Buffer.from([99]))
  t.equals(p.bytesWrote, 1, 'should record the number of bytes written to a the pipe')

  t.deepEquals(p.buffer, Buffer.from([99]))
  t.end()
})
